const tokenSecret = process.env.APP_TOKEN_SECRET;

const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

var middleware = {
    requireJWT: async function(req,res,next){
    	if(!req.headers.authorization) res.status(401).send();
    	else try {
		    const token = req.headers.authorization.split(" ")[1];
		    await jwt.verify(token, tokenSecret, async function (err, payload) {
	        	console.log('Authorized token :', payload);
		        if (payload){
		        	user = await req.context.models.User.findById(payload.userId);
		            if (user) {
		            	req.user = user;
        				next();
        			} else res.status(401).send();
		        } else res.status(401).send();
		    });
		} catch(e) {
			console.log(e);
			res.status(500).send();
		}
	}
}

const router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const authRoutes = require('./auth.js');
router.use('/auth', authRoutes);

const registerRoutes = require('./register.js');
router.use('/register', registerRoutes);

const todoRoutes = require('./todos.js');
router.use('/todos', [middleware.requireJWT, todoRoutes]);

const userRoutes = require('./users.js');
router.use('/users', [middleware.requireJWT, userRoutes]);

const tasklistRoutes = require('./tasklists.js');
router.use('/tasklists', [middleware.requireJWT, tasklistRoutes]);


const swaggerJsdoc = require('swagger-jsdoc');
const options = {
  swaggerDefinition: {
    info: {
      title: 'TaskApp API',
      version: '1.0.0',
      description: 'TaskApp API documentation'
    },
    basePath: '/v1'
  },
  apis: ['./routes/v1/*.js']
};
const specs = swaggerJsdoc(options);

const swaggerUi = require('swagger-ui-express');
router.use('/swagger/', swaggerUi.serve, swaggerUi.setup(specs));

module.exports = router;