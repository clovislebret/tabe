var express = require('express');
var router = express.Router();

/**
 * @swagger
 *
 * definitions:
 *   Tasklist:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       tags:
 *         type: array
 *         items:
 *           type: string
 *       user:
 *         type: string
 */

/**
 * @swagger
 *
 * /tasklists:
 *   get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Tasklists
 *     description: Returns a list of Tasklists
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: tags
 *         type: array
 *         items:
 *           type: string
 *         collectionFormat : multi
 *     responses:
 *       200:
 *         description: Array of tasklists
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Tasklist'
 */
router.get('/', async (req, res) => {
  var tags = req.query.tags;
  if(tags) return res.json(await req.context.models.Tasklist.findByTags(tags));
  return res.json(await req.context.models.Tasklist.find({}));
})


/**
 * @swagger
 * /tasklists/{_id}:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Tasklists
 *     description: Returns a specific todo from a given '_id'
 *     parameters:
 *       - name: _id
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Tasklist'
 *       404:
 *         description: 'Error: Not Found'
 */
router.get('/:_id', async (req, res) => {
  try {
    const todo = await req.context.models.Tasklist.findById(
      req.params._id,
    );
    if(!todo) res.status(404).send();
    return res.json(todo);
  } catch (e) {
    res.status(404).send();
  }
});


/**
 * @swagger
 * /tasklists/{_id}/todos:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Returns a tasklist's todos
 *     parameters:
 *       - name: _id
 *         description: Id of the tasklist.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: todos
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Todo'
 */
router.get('/:_id/todos', async (req, res) => {

  const tasklist = await req.context.models.Tasklist.findById(
    req.params._id
  );

  const todos = await req.context.models.Todo.find({
    tasklist: tasklist._id
  })

  return res.json(todos);
});

/**
 * @swagger
 * /tasklists:
 *    post:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Tasklists
 *     description: Creates a new Tasklist
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Tasklist'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Tasklist'
 */
router.post('/', async (req, res) => {
  try{
    const todo = await req.context.models.Tasklist.create({
      name: req.body.name,
      desc: req.body.desc,
      user: req.body.user
    });
    return res.json(todo);
  } catch (e) {
    res.status(500).send();
  }
});


/**
 * @swagger
 *
 * /tasklists/{_id}:
 *    delete:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Tasklists
 *     description: Deletes a specific tasklist from a given '_id'
 *     parameters:
 *       - name: _id
 *         description: Id of the tasklist.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       204:
 *         description: OK
 *       404:
 *         description: 'Error: Not Found'
 */
router.delete('/:_id', async (req, res) => {
  console.log(req.params._id);
  console.log(req.user);
  try {
    const todo = await req.context.models.Tasklist.findById(
      req.params._id
    );
    await todo.remove();
    res.status(204).send();
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;