var express = require('express');
var router = express.Router();

/**
 * @swagger
 *
 * definitions:
 *   CreateUserRequest:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *         format: password
 *     required:
 *       - username
 *       - email
 *       - password
 *   User:
 *     allOf:
 *       - $ref: '#/definitions/CreateUserRequest'
 *       - type: object
 *         properties:
 *           id:
 *             type: string
 *       - required:
 *         - id
 */

/**
 * @swagger
 *
 * /users:
 *   get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Returns a list of User
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: users
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/User'
 */
router.get('/', async (req, res) => {
  const users = await req.context.models.User.find({});
  return res.json(users);
})

/**
 * @swagger
 * /users/{_id}:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Returns a specific user from a given '_id'
 *     parameters:
 *       - name: _id
 *         description: Id of the username.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           type: object
 *           $ref: '#/definitions/User'
 */
router.get('/:_id', async (req, res) => {
  const user = await req.context.models.User.findById(
    req.params._id
  );
  return res.json(user);
});

/**
 * @swagger
 * /users/{_id}/todos:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Returns a user's list of todos
 *     parameters:
 *       - name: _id
 *         description: Id of the username.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: todos
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Todo'
 */
router.get('/:_id/todos', async (req, res) => {
  try{  
    if(req.user._id.toString().trim() !== req.params._id.toString().trim()) 
      return res.status(401).send();

    const user = await req.context.models.User.findById(
      req.user._id
    );

    const userTodos = await req.context.models.Todo.find({
      user: user._id
    })

    return res.json(userTodos);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

/**
 * @swagger
 * /users/{_id}/tasklists:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Returns a user's list of tasklists
 *     parameters:
 *       - name: _id
 *         description: Id of the tasklist.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: tasklists
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Tasklist'
 */
router.get('/:_id/tasklists', async (req, res) => {
  try{  
    const user = await req.context.models.User.findById(
      req.user._id
    );

    const userTodos = await req.context.models.Tasklist.find({
      user: user._id
    })

    return res.json(userTodos);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

/**
 * @swagger
 * /users:
 *    post:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Creates a new User
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/CreateUserRequest'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'

 */
router.post('/', async (req, res) => {
  const user = await req.context.models.User.create({
    username: req.body.username,
    email: req.body.email
  });

  return res.json(user);
});

/**
 * @swagger
 * /users/{_id}:
 *    put:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Updates a User
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'
 */
router.put('/:_id', async (req, res) => {
  if(req.user._id.toString().trim() !== req.params._id.toString().trim()) 
    res.status(401).send();

  try {
    userModel = req.context.models.User;
    const user = await userModel.findByIdAndUpdate(
      req.params._id
      ,{
        username: req.body.username,
        email: req.body.email
      },{
        new: true
      }
    );
    return res.json(user);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

/**
 * @swagger
 * /users/{_id}:
 *    delete:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Deletes a specific user from a given '_id'
 *     parameters:
 *       - name: _id
 *         description: Id of the user.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       204:
 *         description: OK
 *       404:
 *         description: 'Error: Not Found'
 */
router.delete('/:_id', async (req, res) => {
  if(req.user._id.toString().trim() !== req.params._id.toString().trim()) 
    res.status(401).send();

  try {
    userModel = req.context.models.User;
    const user = await userModel.findByIdAndRemove(
      req.params._id
    );
    res.status(204).send();
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

module.exports = router;