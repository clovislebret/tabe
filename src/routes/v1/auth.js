const tokenSecret = process.env.APP_TOKEN_SECRET;

var express = require('express');
const jwt=require('jsonwebtoken');
var router = express.Router();

/**
 * @swagger
 *
 * definitions:
 *   LoginRequest:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 *         format: password
 *     required:
 *       - username
 *       - password
 * securityDefinitions:
 *   Bearer:
 *     type: apiKey
 *     name: Authorization
 *     in: header
 */

/**
 * @swagger
 *
 * /auth/signin:
 *    post:
 *     tags:
 *       - User, Login
 *     description: Signs a User in
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/LoginRequest'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'
 *       401:
 *         description: Invalid credentials
 *
 */
router.post('/signin', async (req, res) => {
	req.context.models.User.findByLogin(req.body.username).then((user) => {
	    user.comparePassword(req.body.password,(err,isMatch) => {
	        if(isMatch){
	        	console.log(tokenSecret)
	            var token = jwt.sign({userId:user._id}, tokenSecret);
	            res.status(200).json({
	                userId:user._id,
	                username:user.username,
	                token
	            })
	        } 
	        else res.status(401).json({message:'Invalid Password/Username'});
	    })
	}).catch((err)=>{
		console.log('caught', err.message);
	    res.status(401).json({message:'Invalid Password/Username'});
	})
})

module.exports = router;