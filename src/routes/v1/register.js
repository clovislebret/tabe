var express = require('express');
var router = express.Router();

/**
 * @swagger
 *
 * definitions:
 *   RegisterRequest:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 *         format: password
 *       email:
 *         type: string
 *         format: mail
 *     required:
 *       - username
 *       - password
 *       - email
 * securityDefinitions:
 *   Bearer:
 *     type: apiKey
 *     name: Authorization
 *     in: header
 */

/**
 * @swagger
 *
 * /register:
 *    post:
 *     tags:
 *       - User, Register
 *     description: Register a new User
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/RegisterRequest'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/User'
 *       401:
 *         description: Invalid credentials
 *
 */
router.post('/', async (req, res) => {
  const user = await req.context.models.User.create({
    username: req.body.username,
    password: req.body.password,
    email: req.body.email
  });
  console.log(user);
  return res.json(user);
})

module.exports = router;