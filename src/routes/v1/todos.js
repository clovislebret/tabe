var express = require('express');
var router = express.Router();

/**
 * @swagger
 *
 * definitions:
 *   CreateTodoRequest:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       desc:
 *         type: string
 *       tags:
 *         type: array
 *         items:
 *           type: string
 *       user:
 *         type: string 
 *       tasklist:
 *         type: string 
 *       done:
 *         type: boolean
 *   Todo:
 *     allOf:
 *       - $ref: '#/definitions/CreateTodoRequest'
 *       - type: object
 *         properties:
 *           id:
 *             type: string
 *           created:
 *             type: string
 *             format: date-time
 *           updated:
 *             type: string
 *             format: date-time
 */

/**
 * @swagger
 *
 * /todos:
 *   get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Todos
 *     description: Returns a list of Todos
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: tags
 *         type: array
 *         items:
 *           type: string
 *         collectionFormat : multi
 *     responses:
 *       200:
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Todo'
 */
router.get('/', async (req, res) => {
  if(req.query) {
    var params = {}
    for (const [key, param] of Object.entries(req.query)) {
      params[key] = { $in : param }
    }
    return res.json(await req.context.models.Todo.find(params));
  }
  return res.json(await req.context.models.Todo.find({}));
})


/**
 * @swagger
 *
 * /todos/{_id}:
 *    get:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Todos
 *     description: Returns a specific todo from a given '_id'
 *     parameters:
 *       - name: _id
 *         description: Id of the todo.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Todo'
 *       404:
 *         description: 'Error: Not Found'
 */
router.get('/:_id', async (req, res) => {
  try {
    const todo = await req.context.models.Todo.findById(
      req.params._id,
    );
    if(!todo) res.status(404).send();
    return res.json(todo);
  } catch (e) {
    res.status(404).send();
  }
});


/**
 * @swagger
 *
 * /todos:
 *    post:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Todos
 *     description: Creates a new Todo
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/CreateTodoRequest'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Todo'
 */
router.post('/', async (req, res) => {
  console.log(req.body);
  try{
    const todo = await req.context.models.Todo.create({
      name: req.body.name,
      desc: req.body.desc,
      tags: req.body.tags,
      user: req.user._id,
      tasklist: req.body.tasklist
    });
    
    return res.json(todo);
  } catch (e) {
    res.status(500).send();
  }
});

/**
 * @swagger
 * /todos/{_id}:
 *    put:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Users
 *     description: Updates a Todo
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           $ref: '#/definitions/Todo'
 *     responses:
 *       200:
 *         schema:
 *           $ref: '#/definitions/Todo'
 */
router.put('/:_id', async (req, res) => {
  try {
    todoModel = req.context.models.Todo;
    const todo = await todoModel.findByIdAndUpdate(
      req.params._id
      ,{
        done: req.body.done,
        name: req.body.name,
        desc: req.body.desc,
        tags: req.body.tags
      },{
        new: true
      }
    );
    return res.json(todo);
  } catch (e) {
    console.log(e);
    res.status(500).send();
  }
});

/**
 * @swagger
 *
 * /todos/{_id}:
 *    delete:
 *     security:
 *       - Bearer: []
 *     tags:
 *       - Todos
 *     description: Deletes a specific todo from a given '_id'
 *     parameters:
 *       - name: _id
 *         description: Id of the todo.
 *         in: path
 *         required: true
 *         type: string
 *     responses:
 *       204:
 *         description: OK
 *       404:
 *         description: 'Error: Not Found'
 */
router.delete('/:_id', async (req, res) => {
  console.log(req.params._id);
  console.log(req.user);
  try {
    const todo = await req.context.models.Todo.findById(
      req.params._id
    );
    await todo.remove();
    res.status(204).send();
  } catch (e) {
    res.status(500).send();
  }
});

module.exports = router;