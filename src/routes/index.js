const express = require('express');
const router = express.Router();

const v1Routes = require('./v1');

router.get('/', function(req,res){
  res.sendFile(req.context.view_path + 'index.html');
});

router.use('/v1/', v1Routes);

module.exports = router;