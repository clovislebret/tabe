const port = process.env.NODE_PORT;
const refresh = process.env.APP_REFRESH_DB_ON_STARTUP;

const express = require('express');
var cors = require('cors')
const app = express();


app.use(cors());

const modelIndex = require('./models');
const models = modelIndex.models;
app.use( async (req,res,next) => {
    console.log('/' + req.method + ' ' + req.originalUrl);

    req.context = {
      view_path : __dirname + '/views/',
      models
    };

    next();
});

const indexRoutes = require('./routes');
app.use('/', indexRoutes);


const connectDb = modelIndex.connectDb();
connectDb.then(async () => {
	if (refresh) {
    await Promise.all([
      models.User.deleteMany({}),
      models.Tasklist.deleteMany({}),
      models.Todo.deleteMany({})
    ]);
	}

	seed();

	app.listen(
    port, 
    () => console.log('Example app listening on port ' + port + ' !')
  )
});

// TODO : Export into utils
const seed = async () => {

  const user = new models.User({
    username: 'Test',
    password: 'test'
  });

  const tasklist1 = new models.Tasklist({
    name: 'Smash',
    desc: 'SSBU',
    tags: ['Smash'],
    user: user.id
  });

  const tasklist2 = new models.Tasklist({
    name: 'Dev',
    desc: 'Development stuff',
    tags: [],
    user: user.id
  });

  const todo1 = new models.Todo({
    name: 'Add "Add todo"',
    desc: 'Add "Add todo" functionality to front-end',
    tags: ['Task app'],
    user: user.id,
    tasklist: tasklist2.id
  });

  const todo2 = new models.Todo({
    name: 'Todo 2',
    desc: 'Learn matchup vs DDK',
    tags: ['DDK', 'Learn'],
    user: user.id,
    tasklist: tasklist1.id
  });

  const todo3 = new models.Todo({
    name: 'Todo 3',
    desc: 'Blabla',
    tags: ['Inkling', 'Watch', 'EVO'],
    user: user.id,
    tasklist: tasklist1.id
  });
  
  await tasklist1.save().catch( err => {
    console.log('caught', err.message);
  });
  
  await tasklist2.save().catch( err => {
    console.log('caught', err.message);
  });

  await todo1.save().catch( err => {
		console.log('caught', err.message);
  });

  await todo2.save().catch( err => {
    console.log('caught', err.message);
  });

  await todo3.save().catch( err => {
    console.log('caught', err.message);
  });

  await user.save().catch( err => {
		console.log('caught', err.message);
  });

};