const mongoose = require('mongoose');

const tasklistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  desc: {
    type: String
  },
  tags : [String],
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User',
    required: true
  }
});

tasklistSchema.pre('remove', function(next) {
  this.model('Todo').updateMany(
    { tasklist: this._id },
    { $set: { tasklist : null } },
    next
  );
});

tasklistSchema.statics.findByUserId = async function (userId) {
	return await this.findOne({
    user: userId
  });;
};

tasklistSchema.statics.findByTags = async function (tags) {
  return await this.find({
    tags:  {$in: [tags]}
  });;
};

module.exports = mongoose.model('Tasklist', tasklistSchema)