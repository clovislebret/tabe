const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  desc: {
    type: String
  },
  done : { 
    type: Boolean, 
    default: false 
  },
  tags : [String],
  created : { 
    type: Date, 
    default: Date.now, 
    immutable: true 
  },
  updated: { 
    type: Date, 
    default: Date.now 
  },
  tasklist: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Tasklist' 
  },
  user: { 
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User' 
  }
});

todoSchema.statics.findByUserId = async function (userId) {
	return await this.find({
    user: userId
  });;
};

todoSchema.statics.findByTasklistId = async function (listId) {
  return await this.find({
    tasklist: listId
  });;
};

module.exports = mongoose.model('Todo', todoSchema)