const mongoose = require('mongoose');
var bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true
  },
  email: {
    type: String,
    unique: true
  },
  password: String
});

userSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) {return next()};
    bcrypt.hash(user.password,10).then((hashedPassword) => {
        user.password = hashedPassword;
        next();
    })
}, function (err) {
    next(err)
});

userSchema.pre('remove', function(next) {
  this.model('Todo').deleteMany({ user: this._id }, next);
  this.model('Tasklist').deleteMany({ user: this._id }, next);
});

userSchema.methods.comparePassword = function (candidatePassword, next) {    
  bcrypt.compare( candidatePassword, this.password, function(err, isMatch){
      return err ? next(err) : next(null, isMatch);
  })
};

userSchema.statics.findByLogin = async function (login) {
	return await this.findOne({
    username: login
  });;
};

module.exports = mongoose.model('User', userSchema);