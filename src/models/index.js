const mongoose = require('mongoose');

const User = require('./user');
const Tasklist = require('./tasklist');
const Todo = require('./todo');

const connectDb = () => {
	return mongoose.connect(process.env.MONGO_URL);
};

const models = { User, Todo, Tasklist };

module.exports.connectDb = connectDb;
module.exports.models = models;